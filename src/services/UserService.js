import axios from 'axios'
import { useConfigStore } from "@/store/ConfigStore";

const apiClient = axios.create({
  withCredentials: false,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  }
})

export default {
  async registerUser(user) {
    const configStore = useConfigStore()
    await configStore.loadConfiguration()
    return apiClient.post(configStore.configuration.apiUrl + '/book-admin/v1/admins', user)
  },
  async loginUser(loginData) {
    const configStore = useConfigStore()
    await configStore.loadConfiguration()
    console.log(configStore.configuration.apiUrl)
    console.log(loginData)
    return apiClient.post(configStore.configuration.apiUrl + '/v1/login', loginData)
  }
}
