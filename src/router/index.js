// Composables
import { createRouter, createWebHistory } from 'vue-router'
import ErrorDisplay from "@/views/ErrorDisplay.vue";
import { useUserStore } from "@/store/UserStore";

const routes = [
  {
    path: '/',
    component: () => import('@/layouts/default/DefaultLayout.vue'),
    children: [
      {
        path: '',
        name: 'HomePage',
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () => import(/* webpackChunkName: "home" */ '@/views/HomePage.vue')
      },
      {
        path: '/register',
        name: 'UserRegister',
        component: () => import('@/views/UserRegister.vue')
      },
      {
        path: '/login',
        name: 'UserLogin',
        component: () => import('@/views/UserLogin.vue')
      },
      {
        path: '/protected',
        name: 'ProtectedPage',
        meta: { requiresAuth: true },
        component: () => import('@/views/ProtectedPage.vue')
      },
      {
        path: '/error/:error',
        name: 'ErrorDisplay',
        props: true,
        component: ErrorDisplay
      }
    ]
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

router.beforeEach((to, from) => {
  const userStore = useUserStore()
  console.log(userStore.loggedIn)
  if (to.meta.requiresAuth && !userStore.loggedIn) {
    if (from.href) { // <--- If this navigation came from a previous page.
      return false
    } else {  // <--- Must be navigating directly
      return { path: '/' }  // <--- Push navigation to the root route.
    }
  }
})

export default router
